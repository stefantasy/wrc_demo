#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Twist
import time
class aikit_move():
    def __init__(self,mytopic):
        rospy.init_node('robotbase', anonymous=True)
        self.topic=mytopic
        self.pub = rospy.Publisher(self.topic, Twist, queue_size=10)
        self.rate = rospy.Rate(10) # 10hz
        self.cmd_vel=Twist()
    def turn(self,turn_speed,times):   
        t=time.time()
        self.cmd_vel.linear.x=0
        self.cmd_vel.angular.z=turn_speed
        while not rospy.is_shutdown() and (time.time()-t)<times:
            self.pub.publish(self.cmd_vel)
            self.rate.sleep()
    def go(self,go_speed,times):
        t=time.time()
        self.cmd_vel.linear.x=go_speed
        self.cmd_vel.angular.z=0
        while not rospy.is_shutdown() and (time.time()-t)<times:
            self.pub.publish(self.cmd_vel)
            self.rate.sleep()
if __name__ == '__main__':
    t1=aikit_move('turtle1/cmd_vel')
    t2=aikit_move('turtle2/cmd_vel')
    t3=aikit_move('turtle3/cmd_vel')
    while 1:
        t1.go(10,0.001)
        t2.go(10,0.001)
        t3.go(10,0.001)
        t1.turn(1,0.001)
        t2.turn(10,0.001)
        t3.turn(100,0.001)
