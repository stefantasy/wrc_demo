#!/usr/bin/env python
#coding=utf-8
import rospy    #导入rospy库


rospy.init_node('twist_pub', anonymous=True)    #定义节点名称
fabu = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)   #定义一个发布者pub函数，pub名字是自己定义的
rate = rospy.Rate(10) # 设置发布频率为10hz，一秒10次
v=Twist()  #定义一个名字为v的Twist数据格式变量，名字也可以自定义

if __name__ == '__main__':
    while not rospy.is_shutdown() :   #while循环，保证一直发布
        v.linear.x=10.0         #给v变量的x轴线速度赋值为10.0
        v.angular.z=2.0         #给v变量的Z轴角速度赋值为2.0
        fabu.publish(v)         #把v通过发布函数pub发送出去
        rate.sleep()            #按照设置的频率 10hz，给话题发布消息





        



