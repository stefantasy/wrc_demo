#!/usr/bin/env python
import rospy
import serial
from aikit_arm.srv import aikit_arm_srv, aikit_claw_srv, aikit_arm_srvResponse, aikit_claw_srvResponse
rospy.init_node("aikit_arm_service")
Serial = serial.Serial(port="/dev/arm", baudrate=9600, timeout=1)


def arm_callback(req):
    apos = req.apos
    Serial.write('a' + str(apos) + '\r')
    return aikit_arm_srvResponse(req.apos)


def claw_callback(req):
    cpos = req.cpos
    Serial.write('c' + str(cpos) + '\r')
    return aikit_claw_srvResponse(req.cpos)


s = rospy.Service("aikit_arm", aikit_arm_srv, arm_callback)
c = rospy.Service("aikit_claw", aikit_claw_srv, claw_callback)
rospy.spin()
