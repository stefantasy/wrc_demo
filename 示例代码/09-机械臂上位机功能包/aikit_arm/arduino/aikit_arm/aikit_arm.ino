#include <Servo.h>
Servo claw;
#define val A3
//middle pos when startup
int cpos=50; 
//arm apos low:writeMicroseconds(1400) high:writeMicroseconds(2250);
int cpos_top=175;
int cpos_down=5;
int cp=0;
void setup() {
  Serial.begin(9600);
  claw.attach(2); 
  pinMode(val,INPUT);
}
void loop() {
  if(Serial.available()){
    char cmd =Serial.read();
    if (cmd=='c')cpos=Serial.parseInt();
  }
  if(cpos>=100) cpos=100;
  if(cpos<=0) cpos=0;
  cp=map(cpos,0,100,cpos_down,cpos_top);
  claw.write(cp);
}
