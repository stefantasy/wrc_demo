#include <Servo.h>
Servo arm;  
Servo claw;
//middle pos when startup
int apos=50;
int cpos=50; 
int cp=0;
int ap=0; 
//arm apos low:writeMicroseconds(1400) high:writeMicroseconds(2250);
int apos_top=2250;
int apos_down=1400;
//claw cpos low:2500 high 500
int cpos_top=1600;
int cpos_down=500;
void setup() {
  Serial.begin(9600);
  arm.attach(12);
  claw.attach(11); 
}
void loop() {
  if(Serial.available()){
    char cmd =Serial.read();
    if (cmd=='a')apos=Serial.parseInt(); 
    if (cmd=='c')cpos=Serial.parseInt();
  }
  if(apos>=100) apos=100;
  if(apos<=0) apos=0;
  if(cpos>=100) cpos=100;
  if(cpos<=0) cpos=0;
  cp=map(cpos,0,100,cpos_down,cpos_top);
  ap=map(apos,0,100,apos_down,apos_top);
  claw.writeMicroseconds(cp);
  arm.writeMicroseconds(ap);
}
