#!/user/bin/env python
#coding=utf-8
import rospy #导入rospy库
from geometry_msgs.msg import Twist #导入TWist消息数据

rospy.init_node("twist_sub",anonymous=True)  #定义节点名称

def call_back(data):  #定义一个回调函数，当话题有消息时，会执行该函数，并把数据传过来
    x=data.linear.x   #获取x方向线速度
    z=data.angular.z  #获取z轴角速度
    if x>0:
        rospy.loginfo("机器人前进中")
    elif x<0:
        rospy.loginfo("机器人后退中")
    else:
        rospy.loginfo("机器人静止中")

def get_msg(): #定义接受者函数
    rospy.Subscriber("/turtle1/cmd_vel",Twist,call_back,queue_size=10)  #d定义接收者
    rospy.spin()   #持续接受信息
if __name__ == "__main__":
    get_msg()  #执行接收者函数

